#include <iostream>
#include <algorithm>
#include <vector>
#include "functions.h"

using std::tuple;
using std::get;
using std::make_tuple;
using std::cout;
using std::cerr;
using std::endl;
using std::vector;
using std::sort;

Pixel operator*(Pixel x, double y){
    return make_tuple(get<0>(x)*y, get<1>(x)*y, get<2>(x)*y);
}

Pixel operator+(Pixel x, Pixel y){
    return make_tuple(get<0>(x) + get<0>(y), get<1>(x) + get<1>(y), get<2>(x) + get<2>(y));
}

Pixel operator-(Pixel x, Pixel y){
    return make_tuple(max(0, min(get<0>(x) - get<0>(y), 255)), 
            max(0, min(get<1>(x) - get<1>(y), 255)), 
            max(0, min(get<2>(x) - get<2>(y), 255)));
}

Pixel normalize(Pixel x){
    return make_tuple(max(0, min(get<0>(x), 255)),
                      max(0, min(get<1>(x), 255)),
                      max(0, min(get<2>(x), 255)));
}

Pixel sqrt(Pixel x){
    return make_tuple(sqrt(get<0>(x)), sqrt(get<1>(x)), sqrt(get<2>(x)));
}

// Image sobel_x(const Image &source, bool norm){
//     Matrix<double> kernel = {{-1, 0, 1},
//                              {-2, 0, 2},
//                              {-1, 0, 1}};
//     return custom(source, kernel, norm);
// }

// Image sobel_y(const Image &source, bool norm){
//     Matrix<double> kernel = {{ 1,  2,  1},
//                              { 0,  0,  0},
//                              {-1, -2, -1}};
//     return custom(source, kernel, norm);
// }

Sobel sobel_x(const Image &source){
/*    Matrix<double> kernel = {{-1, 0, 1},
                             {-2, 0, 2},
                             {-1, 0, 1}};*/
    Matrix<double> kernel(3, 1);
    kernel(0, 0) = 1;
    kernel(1, 0) = 0;
    kernel(2, 0) = -1;

    return customS(source, kernel);
}

Sobel sobel_y(const Image &source){
    Matrix<double> kernel = {{ -1,  0,  1}};
    return customS(source, kernel);
}

Pixel get_pixel(const Image *source, int row, int col){
    int n_rows = (*source).n_rows, n_cols = (*source).n_cols;
    if(row < 0){
        if(col < 0)
            return (*source)(abs(row) - 1, abs(col) - 1);
        else if (col >= n_cols)
            return (*source)(abs(row) - 1, n_cols - (col - n_cols) - 1);
        else 
            return (*source)(abs(row) - 1, col);
    } else if (row >= n_rows) {
        if(col < 0)
            return (*source)(n_rows - (row - n_rows) - 1, abs(col) - 1);
        else if (col >= n_cols)
            return (*source)(n_rows - (row - n_rows) - 1, n_cols - (col - n_cols) - 1);
        else 
            return (*source)(n_rows - (row - n_rows) - 1, col);
    } else {
        if(col < 0)
            return (*source)(row, abs(col) - 1);
        else if (col >= n_cols)
            return (*source)(row, n_cols - (col - n_cols) - 1);
        else 
            return (*source)(row, col);
    }
}


Image custom(const Image &source, Matrix<double> kernel, bool norm){
    uint h = source.n_rows, w = source.n_cols, kh = kernel.n_rows, kw = kernel.n_cols;
    Image result(h, w);
    if(!(kw%2) || !(kh%2))
        throw std::string("Incorrect kernel");
    uint kh2 = kh/2, kw2 = kw/2;
    for(uint x = 0; x < h; x++)
        for(uint y = 0; y < w; y++){
            double r = 0, g = 0, b = 0;
            for(uint i = 0; i < kh; i++)
                for(uint j = 0; j < kw; j++){
                    double Tr = 0, Tg = 0, Tb = 0;
                    std::tie(Tr, Tg, Tb) = get_pixel(&source, x+i-kh2, y+j-kw2);
                    r += Tr*kernel(i, j);
                    g += Tg*kernel(i, j);
                    b += Tb*kernel(i, j);
                }
            result(x, y) = make_tuple(r, g, b);
            if(norm)
                result(x, y) = normalize(result(x, y));
        }        
    return result;  
}

Sobel customS(const Image &source, Matrix<double> kernel){
    uint h = source.n_rows, w = source.n_cols, kh = kernel.n_rows, kw = kernel.n_cols;
    Sobel result(h, w);
    if(!(kw%2) || !(kh%2))
        throw std::string("Incorrect kernel");
    uint kh2 = kh/2, kw2 = kw/2;
    for(uint x = 0; x < h; x++)
        for(uint y = 0; y < w; y++){
            double r = 0, g = 0, b = 0;
            for(uint i = 0; i < kh; i++)
                for(uint j = 0; j < kw; j++){
                    double Tr = 0, Tg = 0, Tb = 0;
                    std::tie(Tr, Tg, Tb) = get_pixel(&source, x+i-kh2, y+j-kw2);
                    r += Tr*kernel(i, j);
                    g += Tg*kernel(i, j);
                    b += Tb*kernel(i, j);
                }
            result(x, y) = make_tuple(r, g, b);
        }        
    return result;  
}

Image bmp_to_image(BMP *src){
    Image result(src->TellHeight(), src->TellWidth());

    for (uint i = 0; i < result.n_rows; i++) {
        for (uint j = 0; j < result.n_cols; j++) {
            RGBApixel *p = (*src)(j, i);
            result(i, j) = make_tuple(p->Red, p->Green, p->Blue);
        }
    }

    return result;
}

void greyscale(Image &src){
    for (uint i = 0; i < src.n_rows; i++) {
            for (uint j = 0; j < src.n_cols; j++) {
                uint r = get<0>(src(i, j)), g = get<1>(src(i, j)), b = get<2>(src(i, j));
                uint y = 0.299*r + 0.587*g + 0.114*b;
                src(i, j) = make_tuple(y, y, y);
            }
        }
}

void save_image(const Image &im, const char *path)
{
    BMP out;
    out.SetSize(im.n_cols, im.n_rows);

    uint r, g, b;
    RGBApixel p;
    p.Alpha = 255;
    for (uint i = 0; i < im.n_rows; ++i) {
        for (uint j = 0; j < im.n_cols; ++j) {
            std::tie(r, g, b) = im(i, j);
            p.Red = r; p.Green = g; p.Blue = b;
            out.SetPixel(j, i, p);
        }
    }

    if (!out.WriteToFile(path))
        throw std::string("Error writing file ") + std::string(path);
}