#include <string>
#include <vector>
#include <fstream>
#include <cassert>
#include <iostream>
#include <cmath>

#include "classifier.h"
#include "EasyBMP/EasyBMP.h"
#include "liblinear-1.93/linear.h"
#include "argvparser/argvparser.h"
#include "matrix.h"
#include "functions.h"

using std::string;
using std::vector;
using std::ifstream;
using std::ofstream;
using std::pair;
using std::make_pair;
using std::cout;
using std::cerr;
using std::endl;
using std::tuple;
using std::get;
using std::make_tuple;

using CommandLineProcessing::ArgvParser;

const uint n = 1;
const double L = 0.3;

void SVM(vector<float> *img_descriptor, vector<float> *new_img_descriptor);

typedef vector<pair<BMP*, int> > TDataSet;
typedef vector<pair<string, int> > TFileList;
typedef vector<pair<vector<float>, int> > TFeatures;

// Load list of files and its labels from 'data_file' and
// store it in 'file_list'
void LoadFileList(const string& data_file, TFileList* file_list) {
	ifstream stream(data_file.c_str());

	string filename;
	int label;
	
	int char_idx = data_file.size() - 1;
	for (; char_idx >= 0; --char_idx)
		if (data_file[char_idx] == '/' || data_file[char_idx] == '\\')
			break;
	string data_path = data_file.substr(0,char_idx+1);
	
	while(!stream.eof() && !stream.fail()) {
		stream >> filename >> label;
		if (filename.size())
			file_list->push_back(make_pair(data_path + filename, label));
	}

	stream.close();
}

// Load images by list of files 'file_list' and store them in 'data_set'
void LoadImages(const TFileList& file_list, TDataSet* data_set) {
	for (size_t img_idx = 0; img_idx < file_list.size(); ++img_idx) {
			// Create image
		BMP* image = new BMP();
			// Read image from file
		image->ReadFromFile(file_list[img_idx].first.c_str());
			// Add image and it's label to dataset
		data_set->push_back(make_pair(image, file_list[img_idx].second));
	}
}

// Save result of prediction to file
void SavePredictions(const TFileList& file_list,
					 const TLabels& labels, 
					 const string& prediction_file) {
		// Check that list of files and list of labels has equal size 
	assert(file_list.size() == labels.size());
		// Open 'prediction_file' for writing
	ofstream stream(prediction_file.c_str());

		// Write file names and labels to stream
	for (size_t image_idx = 0; image_idx < file_list.size(); ++image_idx)
		stream << file_list[image_idx].first << " " << labels[image_idx] << endl;
	stream.close();
}

void SVM(vector<float> *img_descriptor, vector<float> *new_img_descriptor){
	for(uint i = 0; i < img_descriptor->size(); i++){
		double x = (*img_descriptor)[i];
		for(double lambda = -(n*L); lambda <= n*L; lambda += L){ // real parts
			float function_value = cos(lambda*log(x+0.000001))*sqrt(x*2*cosh(M_PI*lambda)/(cosh(2*M_PI*lambda) + 1));
			new_img_descriptor->push_back(function_value);
		}
		for(double lambda = -(n*L); lambda <= n*L; lambda += L){ // imaginary parts
			float function_value = -sin(lambda*log(x+0.000001))*sqrt(x*2*cosh(M_PI*lambda)/(cosh(2*M_PI*lambda) + 1));
			new_img_descriptor->push_back(function_value);
		}
	}
}

// Extract features from dataset.
// You should implement this function by yourself =)
void ExtractFeatures(const TDataSet& data_set, TFeatures* features) {
	for (size_t i = 0; i < data_set.size(); i++) {

		const uint block_size = 8;
		const double gradient_segment_size = M_PI/8;
		
		Image current = bmp_to_image(get<0>(data_set[i]));
		uint h = current.n_rows, w = current.n_cols;
		Matrix<double> Gd(h, w); // gradient direstions
		Matrix<double> G(h, w); // gradient values
		greyscale(current);
		Sobel Ix = sobel_x(current), Iy = sobel_y(current);
		for(uint x = 0; x < h; x++) // calculating gradients
			for(uint y = 0; y < w; y++){
				G(x, y) = sqrt(pow( get<0>( Ix(x, y) ), 2) + pow( get<0>( Iy(x, y) ), 2));
				Gd(x, y) = atan2(get<0>( Iy(x, y) ), get<0>( Ix(x, y) ));
			}

		uint h_blocks = h/block_size + (h % block_size ? 1 : 0), w_blocks = w/block_size + (w % block_size ? 1 : 0);
		uint Gd_segments = ceil(2*M_PI/gradient_segment_size);
		vector<float> histograms[h_blocks][w_blocks];
		for(uint x = 0; x < h_blocks; x++)
			for(uint y = 0; y < w_blocks; y++)
				for(uint k = 0; k < Gd_segments; k++)
					(histograms[x][y]).push_back(0);

		for(uint x = 0; x < h; x++) // calculating histograms
			for(uint y = 0; y < w; y++){
				uint segment = (Gd(x, y) + M_PI) / gradient_segment_size;
				if(segment == Gd_segments) // Gd == 2pi
					segment--;
				histograms[x/block_size][y/block_size][segment] += G(x, y);
			}

		vector<float> img_descriptor;
		img_descriptor.reserve(h_blocks * w_blocks * Gd_segments);
		for(uint x = 0; x < h_blocks; x++) // normalizing histograms
			for(uint y = 0; y < w_blocks; y++){
					double norm = 0;
					for(uint k = 0; k < Gd_segments; k++)
						norm += pow(histograms[x][y][k], 2);
					norm = pow(norm, 0.5);
					for(uint k = 0; k < Gd_segments; k++)
						histograms[x][y][k] /= norm;
					img_descriptor.insert(img_descriptor.end(), (histograms[x][y]).begin(),
												(histograms[x][y]).end());
				}
		vector<float> new_img_descriptor(2*(2*n+1)*img_descriptor.size());
		SVM(&img_descriptor, &new_img_descriptor);
		features->push_back(make_pair(new_img_descriptor, get<1>(data_set[i])));
	}
}

// Clear dataset structure
void ClearDataset(TDataSet* data_set) {
		// Delete all images from dataset
	for (size_t image_idx = 0; image_idx < data_set->size(); ++image_idx)
		delete (*data_set)[image_idx].first;
		// Clear dataset
	data_set->clear();
}

// Train SVM classifier using data from 'data_file' and save trained model
// to 'model_file'
void TrainClassifier(const string& data_file, const string& model_file) {
		// List of image file names and its labels
	TFileList file_list;
		// Structure of images and its labels
	TDataSet data_set;
		// Structure of features of images and its labels
	TFeatures features;
		// Model which will be trained
	TModel model;
		// Parameters of classifier
	TClassifierParams params;
	
		// Load list of image file names and its labels
	LoadFileList(data_file, &file_list);
		// Load images
	LoadImages(file_list, &data_set);
		// Extract features from images
	ExtractFeatures(data_set, &features);

		// PLACE YOUR CODE HERE
		// You can change parameters of classifier here
	params.C = 0.01;
	TClassifier classifier(params);
		// Train classifier
	classifier.Train(features, &model);
		// Save model to file
	model.Save(model_file);
		// Clear dataset structure
	ClearDataset(&data_set);
}

// Predict data from 'data_file' using model from 'model_file' and
// save predictions to 'prediction_file'
void PredictData(const string& data_file,
				 const string& model_file,
				 const string& prediction_file) {
		// List of image file names and its labels
	TFileList file_list;
		// Structure of images and its labels
	TDataSet data_set;
		// Structure of features of images and its labels
	TFeatures features;
		// List of image labels
	TLabels labels;

		// Load list of image file names and its labels
	LoadFileList(data_file, &file_list);
		// Load images
	LoadImages(file_list, &data_set);
		// Extract features from images
	ExtractFeatures(data_set, &features);

		// Classifier 
	TClassifier classifier = TClassifier(TClassifierParams());
		// Trained model
	TModel model;
		// Load model from file
	model.Load(model_file);
		// Predict images by its features using 'model' and store predictions
		// to 'labels'
	classifier.Predict(features, model, &labels);

		// Save predictions
	SavePredictions(file_list, labels, prediction_file);
		// Clear dataset structure
	ClearDataset(&data_set);
}

int main(int argc, char** argv) {
		// Command line options parser
	ArgvParser cmd;
		// Description of program
	cmd.setIntroductoryDescription("Machine graphics course, task 2. CMC MSU, 2013.");
		// Add help option
	cmd.setHelpOption("h", "help", "Print this help message");
		// Add other options
	cmd.defineOption("data_set", "File with dataset",
		ArgvParser::OptionRequiresValue | ArgvParser::OptionRequired);
	cmd.defineOption("model", "Path to file to save or load model",
		ArgvParser::OptionRequiresValue | ArgvParser::OptionRequired);
	cmd.defineOption("predicted_labels", "Path to file to save prediction results",
		ArgvParser::OptionRequiresValue);
	cmd.defineOption("train", "Train classifier");
	cmd.defineOption("predict", "Predict dataset");
		
		// Add options aliases
	cmd.defineOptionAlternative("data_set", "d");
	cmd.defineOptionAlternative("model", "m");
	cmd.defineOptionAlternative("predicted_labels", "l");
	cmd.defineOptionAlternative("train", "t");
	cmd.defineOptionAlternative("predict", "p");

		// Parse options
	int result = cmd.parse(argc, argv);

		// Check for errors or help option
	if (result) {
		cout << cmd.parseErrorDescription(result) << endl;
		return result;
	}

		// Get values 
	string data_file = cmd.optionValue("data_set");
	string model_file = cmd.optionValue("model");
	bool train = cmd.foundOption("train");
	bool predict = cmd.foundOption("predict");

		// If we need to train classifier
	if (train)
		TrainClassifier(data_file, model_file);
		// If we need to predict data
	if (predict) {
			// You must declare file to save images
		if (!cmd.foundOption("predicted_labels")) {
			cerr << "Error! Option --predicted_labels not found!" << endl;
			return 1;
		}
			// File to save predictions
		string prediction_file = cmd.optionValue("predicted_labels");
			// Predict data
		PredictData(data_file, model_file, prediction_file);
	}
}