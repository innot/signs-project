#pragma once

#include <tuple>
#include "matrix.h"
#include "EasyBMP/EasyBMP.h"

typedef Matrix<std::tuple<uint, uint, uint>> Image;
typedef Matrix<std::tuple<double, double, double>> Sobel;
typedef std::tuple<uint, uint, uint> Pixel;
typedef std::tuple<double, double, double> extPixel;

Pixel operator*(Pixel x, double y); 
Pixel operator+(Pixel x, Pixel y);
Pixel normalize(Pixel x);
Pixel sqrt(Pixel x);
Image custom(const Image &source, Matrix<double> kernel, bool normalize = true);
Sobel customS(const Image &source, Matrix<double> kernel);
Sobel sobel_x(const Image &source);
Sobel sobel_y(const Image &source);
void replaceStrChar(std::string &str, char replace, char ch);
inline int max(int a, int b){
    return a > b ? a : b;
}
inline int min(int a, int b){
    return a > b ? b : a;
}
Pixel get_pixel(const Image *source, int row, int col);
Image bmp_to_image(BMP *src);
void greyscale(Image &src);
void save_image(const Image&, const char*);